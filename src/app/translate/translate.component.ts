import { Component, OnInit } from '@angular/core';
import { createWorker } from 'tesseract.js';
import { GoogleObj } from '../model/solution';
import { GoogletranslateService } from '../services/googletranslate.service';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss'],
})
export class TranslateComponent implements OnInit {
  private translateBtn: any;
  text = '';
  textTranslate = '';
  worker = createWorker({
    logger: (m) => console.log(m),
  });
  constructor(private google: GoogletranslateService) {}
  ngOnInit() {
    this.translateBtn = document.getElementById('translatebtn');
  }
  send() {
    const googleObj: GoogleObj = {
      q: [this.text],
      target: 'vi',
    };
    this.google.translate(googleObj).subscribe(
      (res: any) => {
        console.log(res.data);
        this.textTranslate = res.data.translations[0].translatedText;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async getTextFromImage() {
    await this.worker.load();
    await this.worker.loadLanguage('eng');
    await this.worker.initialize('eng');
    const {
      data: { text },
    } = await this.worker.recognize(
      'https://tesseract.projectnaptha.com/img/eng_bw.png'
    );
    console.log(text);
    this.text = text;
    await this.worker.terminate();
  }
}
