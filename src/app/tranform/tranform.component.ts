import { Component, OnInit } from '@angular/core';
import { createWorker } from 'tesseract.js';

@Component({
  selector: 'app-tranform',
  templateUrl: './tranform.component.html',
  styleUrls: ['./tranform.component.scss'],
})
export class TranformComponent implements OnInit {
  text = "";
  worker = createWorker({
    logger: (m) => console.log(m),
  });
  constructor() {}

  ngOnInit(): void {
    this.getTextFromImage();
  }

  async getTextFromImage() {
    await this.worker.load();
    await this.worker.loadLanguage('eng');
    await this.worker.initialize('eng');
    const {
      data: { text },
    } = await this.worker.recognize(
      'https://tesseract.projectnaptha.com/img/eng_bw.png'
    );
    console.log(text);
    this.text = text;
    await this.worker.terminate();
  }
}
