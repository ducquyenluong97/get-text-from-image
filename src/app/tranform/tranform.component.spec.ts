import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranformComponent } from './tranform.component';

describe('TranformComponent', () => {
  let component: TranformComponent;
  let fixture: ComponentFixture<TranformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
